# Virtual Card Assignment

### You should be install dependencies the project.

```bash
composer install
```

### If you prefer installing it with dummy run
```bash
 php bin/console doctrine:fixures:load
```   


### Route
As you can see application has a routes.
```bash
POST: http://127.0.0.1:8000/virtual-cards
```
```bash
GET: http://127.0.0.1:8000/virtual-cards
```
```bash
DELETE: http://127.0.0.1:8000/virtual-cards/{id}
```

### Notes
You can see everything how i developed your task on the VirtualCardController.php

Maybe you don't know it Vendor2's json is invalid therefore i used file of Vendor1
  