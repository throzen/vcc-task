<?php

namespace App\Controller;

use App\Event\Bucket\BucketEvent;
use App\Facade\VirtualCardFacade;
use App\Response\VirtualCardsResponse;
use App\Service\VirtualCard\Params\CreateVirtualCardParameter;
use App\Service\VirtualCard\VirtualCardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VirtualCardController extends AbstractController
{

    /**
     * @Route("/virtual-cards", name="virtual_card.index", methods={"GET"})
     * @param Request $request
     * @param VirtualCardService $cardService
     * @return JsonResponse
     */
    public function index(Request $request, VirtualCardService $cardService): JsonResponse
    {
        $virtualCards = $cardService->findByCriteria(
            [
                'currency' => $request->get('currency'),
                'vendor' => $request->get('vendor'),
                'createdAt' => $request->get('createdAt'),
            ]
        );

        return $this->json(new VirtualCardsResponse($virtualCards));
    }

    /**
     * @Route("/virtual-cards", name="virtual_card.create", methods={"POST"})
     * @param Request $request
     * @param VirtualCardFacade $cardFacade
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function store(Request $request, VirtualCardFacade $cardFacade): JsonResponse
    {
        $params = new CreateVirtualCardParameter();
        $params->setActivationDate($request->get('activationDate'));
        $params->setExpireDate($request->get('expireDate'));
        $params->setBalance($request->get('balance'));
        $params->setCurrency($request->get('currency'));
        $cardFacade->create($params);

        return $this->json([], Response::HTTP_CREATED);
    }

    /**
     * @Route("/virtual-cards/{id}", name="virtual_card.delete", methods={"DELETE"})
     * @param $id
     * @param VirtualCardService $virtualCardService
     * @return void
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete($id, VirtualCardFacade $virtualCardFacade): JsonResponse
    {
        $virtualCardFacade->delete($id);

        return $this->json([],Response::HTTP_NO_CONTENT);
    }
}
