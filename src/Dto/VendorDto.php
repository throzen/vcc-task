<?php


namespace App\Dto;


class VendorDto
{
    /**
     * @var int
     */
    public $id;

    /** @var string */
    public $name;

}