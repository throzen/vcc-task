<?php


namespace App\Dto;


class VirtualCardDto
{
    /**
     * @var int
     */
    public $id;

    /** @var float */
    public $amount;

    /** @var string */
    public $cardNumber;

    /** @var string */
    public $currency;

    /** @var VendorDto|null */
    public $vendor;
}