<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200119082357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE virtual_card (id INT AUTO_INCREMENT NOT NULL, bucket_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, currency VARCHAR(255) NOT NULL, card_number VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_FF9C85EB84CE584D (bucket_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE virtual_card ADD CONSTRAINT FK_FF9C85EB84CE584D FOREIGN KEY (bucket_id) REFERENCES bucket (id)');
        $this->addSql('ALTER TABLE vendor ADD vendor_key VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE bucket CHANGE vendor_id vendor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bucket ADD CONSTRAINT FK_E73F36A6F603EE73 FOREIGN KEY (vendor_id) REFERENCES vendor (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E73F36A6F603EE73 ON bucket (vendor_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE virtual_card');
        $this->addSql('ALTER TABLE bucket DROP FOREIGN KEY FK_E73F36A6F603EE73');
        $this->addSql('DROP INDEX UNIQ_E73F36A6F603EE73 ON bucket');
        $this->addSql('ALTER TABLE bucket CHANGE vendor_id vendor_id INT NOT NULL');
        $this->addSql('ALTER TABLE vendor DROP vendor_key');
    }
}
