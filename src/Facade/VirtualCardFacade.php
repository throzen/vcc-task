<?php


namespace App\Facade;


use App\Event\Bucket\BucketEvent;
use App\Event\Bucket\BucketParameter;
use App\Service\Bucket\BucketService;
use App\Service\VirtualCard\Params\CreateVirtualCardParameter;
use App\Service\VirtualCard\VirtualCardContext;
use App\Service\VirtualCard\VirtualCardService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VirtualCardFacade
{
    /** @var BucketService */
    protected $bucketService;

    /** @var VirtualCardContext */
    protected $virtualCardContext;

    /** @var VirtualCardService */
    protected $virtualCardService;

    /** @var EventDispatcher */
    protected $eventDispatcher;

    /**
     * VirtualCardFacade constructor.
     * @param BucketService $bucketService
     * @param VirtualCardContext $virtualCardContext
     * @param VirtualCardService $virtualCardService
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        BucketService $bucketService,
        VirtualCardContext $virtualCardContext,
        VirtualCardService $virtualCardService,
        EventDispatcherInterface $dispatcher
    ) {
        $this->bucketService = $bucketService;
        $this->virtualCardContext = $virtualCardContext;
        $this->eventDispatcher = $dispatcher;
        $this->virtualCardService = $virtualCardService;
    }

    /**
     * @param CreateVirtualCardParameter $createCardParams
     * @return \App\Service\VirtualCard\Dto\VirtualCardDto|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(CreateVirtualCardParameter $createCardParams)
    {
        $bucket = $this->bucketService->findBucket(
            $createCardParams->getActivationDate(),
            $createCardParams->getExpireDate()
        );
        if (!$bucket) {
            throw new NotFoundHttpException();
        }

        $response = $this->virtualCardContext->createCard($bucket->getVendor()->getVendorKey());

        $eventParameter = new BucketParameter();
        $eventParameter->setBucket($bucket);
        $eventParameter->setBalance($createCardParams->getBalance());
        $eventParameter->setCurrency($createCardParams->getCurrency());
        $eventParameter->setCardNumber($response->getCardNumber());


        $this->eventDispatcher->dispatch(BucketEvent::CARD_CREATED, new BucketEvent($eventParameter));


        return $response;
    }


    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(int $id)
    {
        $virtualCard = $this->virtualCardService->findById($id);
        if (!$virtualCard) {
            throw new NotFoundHttpException();
        }
        $this->virtualCardService->delete($virtualCard);

        $eventParameter = new BucketParameter();
        $eventParameter->setBucket($virtualCard->getBucket());
        $eventParameter->setBalance($virtualCard->getAmount());
        $eventParameter->setCurrency($virtualCard->getCurrency());
        $eventParameter->setCardNumber($virtualCard->getCardNumber());

        $this->eventDispatcher->dispatch(BucketEvent::CARD_DELETED, new BucketEvent($eventParameter));
    }

}