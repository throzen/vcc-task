<?php

namespace App\Repository;

use App\Entity\Bucket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bucket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bucket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bucket[]    findAll()
 * @method Bucket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BucketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bucket::class);
    }

    // /**
    //  * @return Bucket[] Returns an array of Bucket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /**
     * @param string $startDate
     * @param string $endDate
     * @return Bucket|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByBetweenDate(string $startDate, string $endDate): ?Bucket
    {
        return $this->createQueryBuilder('b')
            ->where('b.startDate >= :startDate')
            ->andWhere('b.endDate <=:endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->orderBy('b.amount', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }


    /**
     * @param Bucket $bucket
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Bucket $bucket): bool
    {
        $this->_em->persist($bucket);
        $this->_em->flush($bucket);

        return true;
    }

    /**
     * @param Bucket $bucket
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Bucket $bucket): bool
    {
        $this->_em->flush($bucket);

        return true;
    }
}
