<?php


namespace App\Transformer;


use App\Dto\VirtualCardDto;
use App\Entity\VirtualCard;

class VirtualCardTransformer
{
    public static function transform(VirtualCard $virtualCard = null): ?VirtualCardDto
    {
        if (!$virtualCard) {
            return null;
        }

        $dto = new VirtualCardDto();
        $dto->amount = $virtualCard->getAmount();
        $dto->cardNumber = $virtualCard->getCardNumber();
        $dto->id = $virtualCard->getId();
        $dto->vendor = VendorTransformer::transform($virtualCard->getVendor());
        $dto->currency = $virtualCard->getCurrency();

        return $dto;

    }

}