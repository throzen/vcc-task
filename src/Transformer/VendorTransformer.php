<?php


namespace App\Transformer;


use App\Dto\VendorDto;
use App\Entity\Vendor;

class VendorTransformer
{

    public static function transform(Vendor $vendor = null): ?VendorDto
    {
        if (!$vendor) {
            return null;
        }

        $dto = new VendorDto();
        $dto->id = $vendor->getId();
        $dto->name = $vendor->getName();

        return $dto;

    }
}