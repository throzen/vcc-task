<?php


namespace App\DependencyInjection\CompilerPass;



use App\Service\VirtualCard\VirtualCardContext;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class LoadVirtualCardVendorCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        $context = $container->findDefinition(VirtualCardContext::class);
        $taggedServices = $container->findTaggedServiceIds('virtual_card_vendor');
        $taggedServiceIds = array_keys($taggedServices);
        foreach ($taggedServiceIds as $taggedServiceId) {
            $context->addMethodCall('addVendor', [new Reference($taggedServiceId)]);
        }
    }
}