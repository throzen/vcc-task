<?php


namespace App\EventListener;


use App\Entity\VirtualCard;
use App\Event\Bucket\BucketEvent;
use App\Service\Bucket\BucketService;
use App\Service\VirtualCard\Currency;
use App\Service\VirtualCard\VirtualCardService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BucketEventListener implements EventSubscriberInterface
{

    /** @var BucketService */
    protected $bucketService;

    /** @var VirtualCardService */
    protected $virtualCardService;

    /**
     * BucketEventListener constructor.
     * @param BucketService $bucketService
     * @param VirtualCardService $cardService
     */
    public function __construct(BucketService $bucketService, VirtualCardService $cardService)
    {
        $this->bucketService = $bucketService;
        $this->virtualCardService = $cardService;
    }


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BucketEvent::CARD_CREATED => 'onCreateCard',
            BucketEvent::CARD_DELETED => 'onDeletedCard',
        ];
    }

    /**
     * @param BucketEvent $bucketEvent
     * @throws \Doctrine\ORM\ORMException
     */
    public function onCreateCard(BucketEvent $bucketEvent)
    {
        $price = $this->bucketService->currencyExchange(
            $bucketEvent->getBucketParameter()->getCurrency(),
            Currency::USD,
            $bucketEvent->getBucketParameter()->getBalance()
        );


        $bucket = $bucketEvent->getBucketParameter()->getBucket();

        $bucket->setAmount($bucket->getAmount() - $price);

        $virtualCard = new VirtualCard();
        $virtualCard->setCurrency($bucketEvent->getBucketParameter()->getCurrency());
        $virtualCard->setAmount($bucketEvent->getBucketParameter()->getBalance());
        $virtualCard->setCardNumber($bucketEvent->getBucketParameter()->getCardNumber());
        $virtualCard->setBucket($bucket);

        $this->virtualCardService->save($virtualCard);

        $this->bucketService->update($bucket);

    }

    /**
     * @param BucketEvent $bucketEvent
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onDeletedCard(BucketEvent $bucketEvent)
    {
        $price = $this->bucketService->currencyExchange(
            $bucketEvent->getBucketParameter()->getCurrency(),
            Currency::USD,
            $bucketEvent->getBucketParameter()->getBalance()
        );

        $bucket = $bucketEvent->getBucketParameter()->getBucket();

        $bucket->setAmount($bucket->getAmount() + $price);

        $this->bucketService->update($bucket);

    }
}