<?php


namespace App\DataFixture;


use App\Entity\Bucket;
use App\Entity\Vendor;
use App\Service\VirtualCard\Vendor\CityVendor;
use App\Service\VirtualCard\Vendor\INGVendor;
use App\Service\VirtualCard\Vendor\StateVendor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BucketFixture extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $ingVendor = new Vendor();
        $ingVendor->setName('ING Vendor');
        $ingVendor->setVendorKey("ING");
        $stateVendor = new Vendor();
        $stateVendor->setName('State Vendor');
        $stateVendor->setVendorKey("STATE");
        $cityVendor = new Vendor();
        $cityVendor->setName('City Vendor');
        $cityVendor->setVendorKey("CITY");

        $manager->persist($ingVendor);
        $manager->persist($stateVendor);
        $manager->persist($cityVendor);

        $manager->flush();


        $stateBucket = new Bucket();
        $stateBucket->setAmount(50000);
        $stateBucket->setStartDate(new \DateTime("2020-01-01"));
        $stateBucket->setEndDate(new \DateTime("2020-01-14"));
        $stateBucket->setVendor($ingVendor);

        $ingBucket = new Bucket();
        $ingBucket->setAmount(200000);
        $ingBucket->setStartDate(new \DateTime("2020-01-01"));
        $ingBucket->setEndDate(new \DateTime("2020-01-30"));
        $ingBucket->setVendor($stateVendor);

        $cityBucket = new Bucket();
        $cityBucket->setAmount(100000);
        $cityBucket->setStartDate(new \DateTime("2020-01-01"));
        $cityBucket->setEndDate(new \DateTime("2020-01-20"));
        $cityBucket->setVendor($cityVendor);

        $manager->persist($ingBucket);
        $manager->persist($cityBucket);
        $manager->persist($stateBucket);

        $manager->flush();
    }
}