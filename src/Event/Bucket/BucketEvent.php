<?php


namespace App\Event\Bucket;


use Symfony\Component\EventDispatcher\Event;

class BucketEvent extends Event
{
    const CARD_CREATED = 'card.created';
    const CARD_DELETED = 'card.deleted';

    /** @var BucketParameter */
    protected $bucketParameter;

    public function __construct(BucketParameter $bucketParameter)
    {
        $this->bucketParameter = $bucketParameter;
    }

    /**
     * @return BucketParameter
     */
    public function getBucketParameter(): BucketParameter
    {
        return $this->bucketParameter;
    }

    /**
     * @param BucketParameter $bucketParameter
     */
    public function setBucketParameter(BucketParameter $bucketParameter): void
    {
        $this->bucketParameter = $bucketParameter;
    }

}