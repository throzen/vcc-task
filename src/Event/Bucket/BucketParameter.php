<?php


namespace App\Event\Bucket;


use App\Entity\Bucket;

class BucketParameter
{
    /** @var Bucket */
    protected $bucket;

    /** @var string */
    protected $currency;

    /** @var float */
    protected $balance;

    /** @var string */
    private $cardNumber;

    /**
     * @return Bucket
     */
    public function getBucket(): Bucket
    {
        return $this->bucket;
    }

    /**
     * @param Bucket $bucket
     */
    public function setBucket(Bucket $bucket): void
    {
        $this->bucket = $bucket;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance(float $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber(string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

}