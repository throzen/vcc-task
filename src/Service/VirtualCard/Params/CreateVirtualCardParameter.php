<?php


namespace App\Service\VirtualCard\Params;


class CreateVirtualCardParameter
{
    /**
     * @var int
     */
    protected $processId;

    /** @var string */
    protected $activationDate;

    /** @var string */
    protected $expireDate;

    /** @var float */
    protected $balance;

    /** @var string */
    protected $currency;

    /** @var string */
    protected $notes;

    /**
     * @return int
     */
    public function getProcessId(): int
    {
        return $this->processId;
    }

    /**
     * @param int $processId
     */
    public function setProcessId(int $processId): void
    {
        $this->processId = $processId;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @param string $activationDate
     */
    public function setActivationDate(string $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getExpireDate(): string
    {
        return $this->expireDate;
    }

    /**
     * @param string $expireDate
     */
    public function setExpireDate(string $expireDate): void
    {
        $this->expireDate = $expireDate;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance(float $balance): void
    {
        $this->balance = $balance;
    }



    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes(string $notes): void
    {
        $this->notes = $notes;
    }


}