<?php


namespace App\Service\VirtualCard\Dto;


class VirtualCardDto
{
    /** @var string */
    protected $cardNumber;

    /** @var int */
    protected $cvv;

    /** @var string */
    protected $referenceCode;

    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber(string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return int
     */
    public function getCvv(): int
    {
        return $this->cvv;
    }

    /**
     * @param int $cvv
     */
    public function setCvv(int $cvv): void
    {
        $this->cvv = $cvv;
    }

    /**
     * @return string
     */
    public function getReferenceCode(): string
    {
        return $this->referenceCode;
    }

    /**
     * @param string $referenceCode
     */
    public function setReferenceCode(string $referenceCode): void
    {
        $this->referenceCode = $referenceCode;
    }

}