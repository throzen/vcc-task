<?php


namespace App\Service\VirtualCard\Contract;


use App\Service\VirtualCard\Dto\VirtualCardDto;

interface VirtualCardVendorInterface
{
    public function canHandle(string $key): bool;

    public function createCard(): ?VirtualCardDto;
}