<?php


namespace App\Service\VirtualCard;


use App\Entity\VirtualCard;
use App\Repository\VirtualCardRepository;

class VirtualCardService
{

    /** @var VirtualCardRepository */
    protected $virtualCardRepository;

    /**
     * VirtualCardService constructor.
     * @param VirtualCardRepository $virtualCardRepository
     */
    public function __construct(VirtualCardRepository $virtualCardRepository)
    {
        $this->virtualCardRepository = $virtualCardRepository;
    }


    /**
     * @param VirtualCard $virtualCard
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(VirtualCard $virtualCard): bool
    {
        return $this->virtualCardRepository->save($virtualCard);
    }

    /**
     * @param VirtualCard $virtualCard
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(VirtualCard $virtualCard): bool
    {
        return $this->virtualCardRepository->update($virtualCard);
    }

    /**
     * @param VirtualCard $virtualCard
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(VirtualCard $virtualCard): bool
    {
        return $this->virtualCardRepository->delete($virtualCard);
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function findByCriteria(array $criteria): array
    {
        return $this->virtualCardRepository->findBy(array_filter($criteria));
    }


    /**
     * @param int $id
     * @return VirtualCard|null
     */
    public function findById(int $id): ?VirtualCard
    {
        return $this->virtualCardRepository->find($id);
    }

}