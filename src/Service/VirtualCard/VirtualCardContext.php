<?php


namespace App\Service\VirtualCard;


use App\Service\VirtualCard\Contract\VirtualCardVendorInterface;

class VirtualCardContext
{

    /** @var VirtualCardVendorInterface[] */
    protected $vendors = [];

    /**
     * @param VirtualCardVendorInterface $virtualCardVendor
     */
    public function addVendor(VirtualCardVendorInterface $virtualCardVendor): void
    {
        $this->vendors[] = $virtualCardVendor;
    }

    /**
     * @param string $vendorKey
     * @return Dto\VirtualCardDto|null
     */
    public function createCard(string $vendorKey)
    {
        foreach ($this->vendors as $vendor) {
            if ($vendor->canHandle($vendorKey)) {
                return $vendor->createCard();
            }
        }
        throw new \InvalidArgumentException(
            'Unsupported vendor type : ' . $vendorKey
        );
    }


}