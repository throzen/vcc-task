<?php


namespace App\Service\VirtualCard\Transformer;


use App\Service\VirtualCard\Dto\VirtualCardDto;
use Psr\Http\Message\ResponseInterface;

class INGResponseTransformer
{

    public static function transform(ResponseInterface $responseInterface): ?VirtualCardDto
    {
        $responseBody = $responseInterface->getBody()->getContents();

        if (!$responseBody) {
            return null;
        }

        $response = json_decode($responseBody);

        $dto = new VirtualCardDto();
        $dto->setCardNumber($response->number);
        $dto->setCvv($response->securityCode);
        $dto->setReferenceCode($response->refCode);

        return $dto;
    }

}