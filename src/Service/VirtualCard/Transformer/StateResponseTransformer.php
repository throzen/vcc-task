<?php


namespace App\Service\VirtualCard\Transformer;


use App\Service\VirtualCard\Dto\VirtualCardDto;
use Psr\Http\Message\ResponseInterface;

class StateResponseTransformer
{

    public static function transform(ResponseInterface $response): ?VirtualCardDto
    {
        $responseJson = $response->getBody()->getContents();

        if (!$responseJson) {
            return null;
        }

        $_response = json_decode($responseJson);

        $dto = new VirtualCardDto();
        $dto->setCardNumber($_response->cardNo);
        $dto->setCvv($_response->cvv);
        $dto->setReferenceCode($_response->refString);

        return $dto;
    }

}