<?php


namespace App\Service\VirtualCard\Vendor;


use App\Service\INGVendor\INGVendorService;
use App\Service\VirtualCard\Contract\VirtualCardVendorInterface;
use App\Service\VirtualCard\Dto\VirtualCardDto;
use App\Service\VirtualCard\Transformer\INGResponseTransformer;

class INGVendor implements VirtualCardVendorInterface
{
    const VENDOR_KEY = "ING";

    /** @var INGVendorService */
    protected $ingVendorService;

    /**
     * INGVendor constructor.
     * @param INGVendorService $ingVendorService
     */
    public function __construct(INGVendorService $ingVendorService)
    {
        $this->ingVendorService = $ingVendorService;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function canHandle(string $key): bool
    {
        return $key == self::VENDOR_KEY;
    }

    /**
     * @return VirtualCardDto|null
     */
    public function createCard(): ?VirtualCardDto
    {
        $response = $this->ingVendorService->makeRequest()->getResponse();

        return INGResponseTransformer::transform($response);
    }
}