<?php


namespace App\Service\VirtualCard\Vendor;


use App\Service\StateVendor\StateVendorService;
use App\Service\VirtualCard\Contract\VirtualCardVendorInterface;
use App\Service\VirtualCard\Dto\VirtualCardDto;
use App\Service\VirtualCard\Transformer\StateResponseTransformer;

class StateVendor implements VirtualCardVendorInterface
{
    const VENDOR_KEY = "STATE";

    /** @var StateVendorService */
    protected $stateVendorService;

    /**
     * StateVendor constructor.
     * @param StateVendorService $stateVendorService
     */
    public function __construct(StateVendorService $stateVendorService)
    {
        $this->stateVendorService = $stateVendorService;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function canHandle(string $key): bool
    {
        return $key == self::VENDOR_KEY;
    }

    /**
     * @return VirtualCardDto|null
     */
    public function createCard(): ?VirtualCardDto
    {
        $response = $this->stateVendorService->makeRequest()->getResponse();

        return StateResponseTransformer::transform($response);
    }
}