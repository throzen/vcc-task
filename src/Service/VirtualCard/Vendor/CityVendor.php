<?php


namespace App\Service\VirtualCard\Vendor;


use App\Service\CityVendor\CityVendorService;
use App\Service\VirtualCard\Contract\VirtualCardVendorInterface;
use App\Service\VirtualCard\Dto\VirtualCardDto;
use App\Service\VirtualCard\Transformer\CityResponseTransformer;

class CityVendor implements VirtualCardVendorInterface
{
    const VENDOR_KEY = "CITY";

    /** @var CityVendorService */
    protected $cityVendorService;

    public function __construct(CityVendorService $cityVendorService)
    {
        $this->cityVendorService = $cityVendorService;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function canHandle(string $key): bool
    {
        return $key == self::VENDOR_KEY;
    }


    /**
     * @return VirtualCardDto|null
     */
    public function createCard(): ?VirtualCardDto
    {
        $response = $this->cityVendorService->makeRequest()->getResponse();

        return CityResponseTransformer::transform($response);
    }


}