<?php


namespace App\Service\Bucket;


use App\Entity\Bucket;
use App\Repository\BucketRepository;
use GuzzleHttp\Client;

class BucketService
{
    /** @var BucketRepository */
    protected $bucketRepository;

    /**
     * BucketService constructor.
     * @param BucketRepository $bucketRepository
     */
    public function __construct(BucketRepository $bucketRepository)
    {
        $this->bucketRepository = $bucketRepository;
    }


    /**
     * @param string $startDate
     * @param string $endDate
     * @return Bucket|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBucket(string $startDate, string $endDate)
    {
        return $this->bucketRepository->findOneByBetweenDate($startDate, $endDate);
    }

    /**
     * @param $id
     * @return Bucket|null
     */
    public function findBucketById($id):?Bucket
    {
        return $this->bucketRepository->find($id);
    }

    /**
     * @param Bucket $bucket
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(Bucket $bucket): bool
    {
        return $this->bucketRepository->save($bucket);
    }

    /**
     * @param Bucket $bucket
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Bucket $bucket)
    {
        return $this->bucketRepository->update($bucket);
    }


    /**
     * @param string $from
     * @param string $to
     * @param float $value
     * @return float
     */
    public function currencyExchange(string $from, string $to, float $value): float
    {
        $url = "http://www.mocky.io/v2/5da5e0e3340000dd1a632d70";

        $client = new Client();
        $response = $client->get($url);

        $rate = json_decode($response->getBody()->getContents());

        $fromRate = 1;
        if ($from == "USD") {
            $fromRate = $value / 1;
        } elseif ($from == "EUR") {
            $fromRate = $value / $rate->EURUSD;
        } elseif ($from == "TRY") {
            $fromRate = $value / $rate->EURTRY;
        }


        $toRate = 1;
        if ($to == "USD") {
            $toRate = $value / 1;
        } elseif ($to == "EUR") {
            $toRate = $value / $rate->EURUSD;
        } elseif ($to == "TRY") {
            $toRate = $value / $rate->EURTRY;
        }

        return round(($value / $toRate) * $fromRate, 2);

    }

}