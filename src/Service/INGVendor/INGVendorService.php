<?php


namespace App\Service\INGVendor;

class INGVendorService extends AbstractService
{
    const URL = 'http://www.mocky.io/v2/5da5dcd2340000221a632d50';

    protected function buildRequest(): void
    {
        $this->request = $this->client->buildRequest(self::URL, 'GET');
    }
}