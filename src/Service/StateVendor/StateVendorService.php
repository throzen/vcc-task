<?php


namespace App\Service\StateVendor;

class StateVendorService extends AbstractService
{
    const URL = 'http://www.mocky.io/v2/5da5dbd33400001628632d49';

    protected function buildRequest(): void
    {
        $this->request = $this->client->buildRequest(self::URL, 'GET');
    }
}