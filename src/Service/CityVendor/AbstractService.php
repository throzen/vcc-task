<?php


namespace App\Service\CityVendor;


use App\Service\CityVendor\Client\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractService
{
    /** @var Client */
    protected $client;

    /** @var RequestInterface */
    protected $request;

    /** @var ResponseInterface */
    protected $response;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function makeRequest()
    {
        $this->buildRequest();

        $this->response = $this->client->send($this->request);
        return $this;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    abstract protected function buildRequest(): void;

}