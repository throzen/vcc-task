<?php


namespace App\Service\CityVendor\Client;


use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class Client
{
    /** @var \GuzzleHttp\Client */
    protected $client;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @param $uri
     * @param $method
     * @param array $params
     * @param array $headers
     * @return RequestInterface
     */
    public function buildRequest($uri, $method, $params = [], $headers = [])
    {
        return new Request($method, $uri, array_merge(["Content-Type" => 'application/json'], $headers), json_encode($params));
    }

    /**
     * @param RequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(RequestInterface $request)
    {
        return $this->client->send($request);
    }

}