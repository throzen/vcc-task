<?php


namespace App\Response;


use App\Entity\VirtualCard;
use App\Transformer\VirtualCardTransformer;

class VirtualCardsResponse implements \JsonSerializable
{
    /** @var array */
    protected $virtualCards = [];

    /**
     * VirtualCardsResponse constructor.
     * @param VirtualCard[] $virtualCards
     */
    public function __construct($virtualCards)
    {
        foreach ($virtualCards as $virtualCard) {
            $this->virtualCards[] = VirtualCardTransformer::transform($virtualCard);
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'virtualCards' => $this->virtualCards,
        ];
    }
}